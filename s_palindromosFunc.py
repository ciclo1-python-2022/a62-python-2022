
# Identificar Palíndromos
# Frases o Palabras que se leen igual de izquierda a derecho que de derecha a izquierda.
# oso  -  ana - ojo - ala - anilina -  luz azul - radar - yo hago yoga hoy - anita lava la tina 
# Dábale arroz a la zorra el abad

# -----------------------------------------------
# Declarar o definir funcione(s)
def palindromo(palabraFrase):
    # palabraFrase = ""
    palabraFrase = palabraFrase.lower()
    palabraFrase = palabraFrase.replace(" ", "")
    palabraFrase = palabraFrase.replace("á", "a")
    palabraFrase = palabraFrase.replace("é", "e")
    palabraFrase = palabraFrase.replace("í", "i")
    palabraFrase = palabraFrase.replace("ó", "o")
    palabraFrase = palabraFrase.replace("ú", "u")
    palabraFrase = palabraFrase.replace("\t", "")
    palabraFraseInvertida = palabraFrase[::-1]

    if palabraFrase == palabraFraseInvertida:
        return True  # La palabra / frase es un palíndromo.
    else:
        return False # La palabra / frase NO es un palíndromo.
    



# -----------------------------------------------
# Programa a ejecutar...

frase = ""

while frase.lower().strip() != "salir":
    # frase = "Tripulantes"
    frase = input("\nDigite una palabra o frase palíndromo (\"salir\" para terminar): \n")
    if palindromo(frase):
        print("\nLa palabra o frase \"" + frase + "\" es un Palíndromo.\n")
    else:
        print("\nLa palabra o frase \"" + frase + "\" NO es un Palíndromo.\n")



# a = "Tripulantes"
# a.strip()
# a.replace()

