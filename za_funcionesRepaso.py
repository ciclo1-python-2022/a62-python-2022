

# ----------------------------
# Definir o Declarar la Función
def par_impar(num):
    
    modulo = num % 2
    
    if modulo == 0:
        parImpar = True
    else:
        parImpar = False
        
    return parImpar

# Ejecución del Programa
a = 66
b = par_impar(a)
print(f"La respuesta es: {b}\n")


print(par_impar(43), "\n")



# -----------------------------
def peso_a_euro(pesos):
    
    pesos1Euro = 4500
    
    euros = pesos * 1 / pesos1Euro
    
    return euros


print(f"\nEuro = {peso_a_euro(100000)}\n")
print(f"\nEuro = {peso_a_euro(1000000)}\n")

