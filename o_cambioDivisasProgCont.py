
# Programa Conversión o Cambio de Divisas

# Programa para validar el número máximo de intentos para usuario y contraseña
# ---------------------------
userGuardado = "tripulantes"
contrGuardada = "abc123"
menu = """
###### Menú de Opciones - Conversor de Divisas####

1. Convertir de Pesos Colombianos a Dólares.
2. Convertir de Dólares a Pesos Colombianos.
3. Convertir de Pesos Colombianos a Euros.
4. Convertir de Euros a Pesos Colombianos.
5. Registre el valor del dólar respecto al peso colombiano.
6. Registre el valor del euro respecto al peso colombiano.
7. Mostrar el histórico del dólar, respecto al peso colombiano.
8. Mostrar el histórico del euro, respecto al peso colombiano.

0. Para Terminar o Salir.

Seleccione una opción: """
# ---------------------------

print("\nInicio del Programa...\n")
cont = 1
userPswCheck = False
cantIntentos = 3

# while cont <= 3 and userPswCheck == False:
while cont <= cantIntentos and (not userPswCheck):
    
    usuario = input("Digite el usuario: ")
    contrasegna = input("Digite la contraseña: ")

    if usuario == userGuardado and contrasegna == contrGuardada:
        print("Usuaio y Contraseña Validados...")
        userPswCheck = True
    else:
        print("\nUsuario y/o Contraseña Incorrecto... \n" +
              "Intente Nuevamente... le quedan: ", cantIntentos - cont, "intentos...\n")
        cont += 1  
print("\nSalió del While\n")


# if userPswCheck == True:
if userPswCheck:
    print("\nBienvenido Ingresó al Conversor de Divisas")
    # ---------------------------------------------------
    # Programa Completo...
    # menuOpcion = ""
    menuOpcion = str()
    # pesos1Dolar = 0.0
    pesos1Dolar = float()
    # pesos1Euro = 0.0
    pesos1Euro = float()
    histDolar = str()
    histEuro = str()

    while menuOpcion != "0":

        menuOpcion = input(menu)

        if menuOpcion == "1":
            print("\n################################################################")
            print("\nSeleccionó la opción 1 => Conv. Pesos a Dólares \n")

            if pesos1Dolar != 0:
                # pass
                cantPesos = float(input("Digite la cantidad de Pesos Colombianos que tiene: "))
                cantDolares = cantPesos * 1 / pesos1Dolar
                print("\nUsted tiene USD$", cantDolares, "dólares.\n")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                # pass
                print("\nPor favor, registre el precio del dólar en la opción 5 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "2":
            print("\n################################################################")
            print("\nSeleccionó la opción 2 => Conv. Dólares a Pesos ")

            if pesos1Dolar != 0:
                # pass
                cantDolares = float(input("Digite la cantidad de Dólares que tiene: "))
                cantPesos = cantDolares * pesos1Dolar / 1
                print("\nUsted tiene COP$", cantPesos, "pesos.\n")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                # pass
                print("\nPor favor, registre el precio del dólar en la opción 5 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "3":
            print("\n################################################################")
            print("\nSeleccionó la opción 3 => Conv. Pesos a Euros ")
            
            if pesos1Euro != 0:
                # pass
                cantPesos = float(input("Digite la cantidad de Pesos Colombianos que tiene: "))
                cantEuros = cantPesos * 1 / pesos1Euro
                print("\nUsted tiene EUR€", cantEuros, "euros.\n")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                # pass
                print("\nPor favor, registre el precio del euro en la opción 6 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "4":
            print("\n################################################################")
            print("vSeleccionó la opción 4 => Conv. Euros a Pesos ")
            
            if pesos1Euro != 0:
                # pass
                cantEuros = float(input("Digite la cantidad de Euros que tiene: "))
                cantPesos = cantEuros * pesos1Euro / 1
                print("\nUsted tiene COP$", cantPesos, "pesos.\n")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                # pass
                print("\nPor favor, registre el precio del euro en la opción 6 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "5":
            print("\n################################################################")
            print("\nSeleccionó la opción 5 => Registrar Valor del Dolar ")

            copiaDolar = pesos1Dolar
            pesos1Dolar = input("\nDigite la equivalencia de 1 Dólar en pesos (digite \"salir\" para cancelar): ")
            if pesos1Dolar != "salir" and pesos1Dolar != "SALIR" and pesos1Dolar != "Salir" and pesos1Dolar != "":
                histDolar += pesos1Dolar + ", "
                pesos1Dolar = float(pesos1Dolar)
                print("\nSe ha registrado correctamente...")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                pesos1Dolar = copiaDolar
                print("\nHa decidido cancelar el registro del Dólar...")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "6":
            print("\n################################################################")
            print("\nSeleccionó la opción 6 => => Registrar Valor del Euro ")

            copiaEuro = pesos1Euro
            pesos1Euro = input("\nDigite la equivalencia de 1 Euro en pesos (digite \"salir\" para cancelar): ")
            if pesos1Euro != "salir" and pesos1Euro != "SALIR" and pesos1Euro != "Salir" and pesos1Euro != "":
                histEuro += pesos1Euro + ", "
                pesos1Euro = float(pesos1Euro)
                print("\nSe ha registrado correctamente...")
                continuar = input("\nPresione Enter para Continuar...")
            else:
                pesos1Euro = copiaEuro
                print("\nHa decidido cancelar el registro del Euro...")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "7":
            print("\n################################################################")
            print("\nSeleccionó la opción 7 => Mostrar Histórico del Dólar ")

            if pesos1Dolar != 0:
                print("\nEl Histórico del Dólar es: ")
                print(histDolar)
                continuar = input("\nPresione Enter para Continuar...")
            else:
                print("\nPor favor, registre el precio del dólar en la opción 5 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "8":
            print("\n################################################################")
            print("\nSeleccionó la opción 8 => Mostrar Histórico del Euro ")

            if pesos1Euro != 0:
                print("\nEl Histórico del Euro es: ")
                print(histEuro)
                continuar = input("\nPresione Enter para Continuar...")
            else:
                print("\nPor favor, registre el precio del euro en la opción 6 del menú.\n")
                continuar = input("\nPresione Enter para Continuar...")

            print("\n################################################################\n")
        elif menuOpcion == "0":
            print("\n################################################################")
            print("\nSeleccionó Terminar o Salir ")
            menuOpcion = input("Por favor, confirme que desea salir (S/N): ")
            if menuOpcion == "S" or menuOpcion == "s" or menuOpcion == "":
                menuOpcion = "0"
            print("\n################################################################\n")
        else:
            print("\n################################################################")
            print("Por favor seleccione una de las opciones válidas del menú...")
            continuar = input("\nPresione Enter para Continuar...")
            print("\n################################################################\n")

    # ---------------------------------------------------
else:
    print("\nHa agotodo el número máximo de intentos...\n Espere 10min para intentar nuevamente...")

print("\nFin del Programa...\n")
