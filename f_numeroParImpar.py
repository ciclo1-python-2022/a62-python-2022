
# Programa para identificar si un número es par o impar

# Entrada(s)
numero = 6
numero = float(input("Digite un número: "))

# # Transformación
# modulo = numero % 2

# # Salida(s)
# # Si el módulo es igual a cero entonces el número es par
# if modulo == 0:
#     print("Repuesta True")
#     print("El número", numero, "es Par")
# else:
#     print("Respuesta False")
#     print("El número", numero, "es Impar")



# -----------------------------------------------------------
# Otra forma...

# Transformación
divEnt = numero // 2   # división entera
comprobar = divEnt * 2

# Salida(s)
# Si el módulo es igual a cero entonces el número es par
if numero == comprobar:
    print("Repuesta True")
    print("El número", numero, "es Par")
else:
    print("Respuesta False")
    print("El número", numero, "es Impar")





# -----------------------------------------------------------
# Otra forma...

# Transformación
# divEnt = numero // 2   # división entera
divEnt = int(numero / 2)   # división entera
comprobar = divEnt * 2

# Salida(s)
# Si el módulo es igual a cero entonces el número es par
if numero == comprobar:
    print("Repuesta True")
    print("El número", numero, "es Par")
else:
    print("Respuesta False")
    print("El número", numero, "es Impar")



# numero = 6
# if numero % 2 == 0:
#     print("Número par")
    

# numero = 6
# if numero % 2 == 0:
#     print("Número par")


# numero = 6
# if int(numero / 2) * 2 == numero:
#     print("Número par")


# numero = 6
# if (numero // 2) * 2 == numero:
#     print("Número par")


# numero = 6
# if (numero // 2) == numero / 2:
#     print("Número par")