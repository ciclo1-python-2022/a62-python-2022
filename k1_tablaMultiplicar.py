
# Programa para generar una tabla de multiplicación

tablaMultip = 1
menu = """
######################################################################

Para finalizar el programa digite un valor negativo o cero(0)

Digite el número al cual desea generar la tabla de multiplicar: """

while tablaMultip > 0:

    tablaMultip = int(input(menu))
    print("###### Tabla de Multiplicar del", tablaMultip, "######")
    for cont in range(1, 21):
        print(tablaMultip, "x", cont, "=", tablaMultip * cont)
    
    
print("\n######################################################################")
print("\nFin del Programa...\n")