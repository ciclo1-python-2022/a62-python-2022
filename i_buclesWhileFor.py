
# Ejemplos de Bucles While y For

contador = 0
bandera = True

print("\nWhile utilizando bandera contador >= 0 and bandera\n")
while contador >= 0 and bandera:
    # contador = contador + 1
    contador += 1
    print("Contador va en: ", contador)
    if contador == 3:
        bandera = False
    # contador += 1

print("Variable contador 1 termina en = ", contador)
print("\nWhile utilizando únicamente contador <= 10\n")

contador = 0
while contador <= 3:
    # contador = contador + 1
    # contador += 1
    print("Contador va en: ", contador)
    # if contador == 10:
    #     bandera = False
    contador += 1
print("Variable contador 2 termina en = ", contador)

print("\nInicia for contador in range(4)\n")
# for(i = 0; i < 4; i++)
for contador in range(4):
    print("Variable contador =", contador)

print("Variable contador 3 termina en = ", contador)

print("\nInicia for contador in range(1, 4)\n")

for contador in range(1, 4):
    print("Variable contador =", contador)


print("\nInicia for contador in range(1, 6, 2)\n")

for contador in range(1, 6, 2):
    print("Variable contador =", contador)
print("Variable contador 4 termina en = ", contador)


print("\nInicia for contador in range(6, 1, -2)\n")

for contador in range(6, 1, -2):
    print("Variable contador =", contador)
print("Variable contador 5 termina en = ", contador)


print("\nInicia for contador in range(6, 1, -1)\n")

inicia = 6
termina = 0
decreciente = -1
# for contador in range(6, 1, -1):
for contador in range(inicia, termina, decreciente):
# for contador in range(termina, inicia, decreciente):
    print("Variable contador =", contador)
print("Variable i 6 termina en = ", contador)



print("\nCódigo para deletrear una cadena de caracteres...\n")
deletrea = "Tripulantes"
# deletrea = input("Digite el texto a deletrear: ")
cont = 0
for i in deletrea:
    # cont = cont + 1
    # cont += 1
    print("Letra ", cont, ":", i)
    cont += 1



print("\nCódigo para deletrear una cadena de caracteres horizontalmente...\n")
deletrea = "Tripulantes"
# deletrea = input("Digite el texto a deletrear: ")
# acumul = ""
acumul = str()
for i in deletrea:
    # acumul = acumul + i + ", "
    acumul += i + ", "
    print(acumul)


print("\nFin del programa...\n")


