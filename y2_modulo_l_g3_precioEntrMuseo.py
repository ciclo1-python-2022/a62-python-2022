
# Programa para identificar el pago de entrada al Museo del Oro según rango de edad
# Si es menor de 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000   => a partir de 60 años.
# Los demás pagan $60.000   => mayor de edad (18) y menor de 60 años.
# Si el visitante tiene una estura mayor a metro y medio, incrementa el costo en $3.000
# Si es fin de semana o festivo el costo de la entrada incrementa en 30%
# Visitante en familia debe indicar la cantidad de integrantes y establecer el costo total
# Dividir el código en 2 funciones: la primera calcular el precio por usuario
# La segunda función que ingrese la cantidad de usuario por familia y un descuento a aplicar.
# La segunda función debe llamar a la primera para calcular el costo de cada usuario
# El museo decide recibir lista de estudiantes/empleados de una institución
# Se debe componer de una lista de diccionarios que contenga los datos de los visitantes (edad y estatura).

def precioEntradaMuseo(edad, estatura, fds):
    if edad < 12:
        precEntrMuseo = 10000

    # elif edad >= 12 and edad < 18:
    elif edad < 18:
        precEntrMuseo = 20000

    # elif edad >= 18 and edad < 60:
    elif edad < 60:
        precEntrMuseo = 60000

    else:
        precEntrMuseo = 30000

    if estatura > 1.5:
        # precEntrMuseo = precEntrMuseo + 3000
        precEntrMuseo += 3000

    # if fds == True:
    if fds:
        # precEntrMuseo = precEntrMuseo + (precEntrMuseo * 30 / 100)
        # precEntrMuseo = precEntrMuseo * (1 + 0.3)
        # precEntrMuseo = precEntrMuseo * (1.3)
        precEntrMuseo *= (1.3)
    
    return precEntrMuseo



# def totalPagar(cantIntegFlia, descuento, fds):
def totalPagar(listaVisitantes, descuento, fds):
    precioTotal = 0

    # for cont in range(cantIntegFlia):
    for cont in listaVisitantes:

        # edad = 6
        # texto = "\nDigite la edad del visitante " + str(cont + 1) + " al Museo: "
        # edad = int(input(texto))
        edad = cont["Edad"]
        # texto2 = "Digite la estura del visitante " + str(cont + 1) + " (m): "
        # estatura = float(input(texto2))
        estatura = cont["Estatura"]

        # Transformación
        # llamado a la función...
        precEntrMuseo = precioEntradaMuseo(edad, estatura, fds)

        # Salida(s)
        # precEntrMuseo
        # print("Precio entrada al Museo de Oro intetegrante:", (cont + 1), "es de $", precEntrMuseo, "Pesos\n")
        print("Precio entrada al Museo de Oro intetegrante:", (cont["Nombre"]), "es de $", precEntrMuseo, "Pesos\n")
        precioTotal += precEntrMuseo

    print("Precio entrada al museo de los integrantes es: $", precioTotal, "pesos, Antes del Descuento.")
    # precioTotal = precioTotal - precioTotal * descuento / 100
    # precioTotal = precioTotal * (1 - descuento / 100)
    precioTotal *= (1 - descuento / 100)

    return precioTotal
