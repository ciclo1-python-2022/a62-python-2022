
# Programa para comparar el precio de 2 productos

# Entrada(s)
# precioProd1 = 5
precioProd1 = float(input("Digite el precio del producto 1: "))
# precioProd2 = 6
precioProd2 = float(input("Digite el precio del producto 2: "))


# Tranformación - Salida(s)

print("------")
print("Inicia if precioProd1 < precioProd2")
if precioProd1 < precioProd2:
    print("La respuesta es True del primer condicional.")
    print("El precio del producto 1 es menor que el precio del producto 2.")
elif precioProd1 > precioProd2:
    print("La respuesta es True del condicional else if.")
    print("EL precio del producto 2 es menor que el precio del pruducto 1.")
else:
# elif precioProd1 == precioProd2:
    print("La respuesta es False del condicional.")
    print("El precio del producto 1 es igual al precio del producto 2.")
# else:
    print("Otro caso...")

print("fin if precioProd1 < precioProd2")

print("------")