
# Ejemplo Establecer Volores por defecto en una función

# -----------------------------
# Definir o Declarar la Función

# def valor(parametro):
def valor(parametro = 6):
    print(f"\nEl valor del parámetro = {parametro}")
    return True


# -----------------------------
# Ejecución del Programa...
# b = valor()
# print(f"\nRetorno de la función: {b} \n")
print(f"\nRetorno de la función: {valor()} \n")

print(f"Retorno de la función: {valor(10)} \n")

# a = input("\nDigiten un valor: ")
# b = valor(a)
# print(f"\nRetorno de la función: {b} \n")


# Impresión con formato ejemplo 1
x = "Cesar"
print(f"\nHola {x}\n")

# Impresión con formato ejemplo 2
y = "Luz Mary"
print("\nHola {0}\n".format(y))

# Impresión con formato ejemplo 3
z = "JAS"
w = "\nHola {0}\n"
print(w.format(z))


