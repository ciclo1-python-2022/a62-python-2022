
# Programa para Convertir Litros a Mililitros

# Entrada(s)
# cantLitTengo = 2   # 2 Litros
# cantLitTengo = input("Digite la cantidad de Litros a convertir: ")
# cantLitTengo = float(cantLitTengo)
cantLitTengo = float(input("Digite la cantidad de Litros a convertir: "))
# cantLitTengo = float(input())

# Transformación
cantMilObtengo = cantLitTengo * 1000 / 1
cant2Recip = cantMilObtengo / 2  # para separar en 2 recipientes

# Salida(s)
print("La cantidad de militros es: ")
print(cantMilObtengo)
print("La cantidad para cada uno de los recipientes es:")
print(cant2Recip)