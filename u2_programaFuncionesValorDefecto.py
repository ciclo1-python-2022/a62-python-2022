
# Importar el Módulo => Hacer el llamado al Archivo que tienen la función
import u2_moduloFuncionesValorDefecto

# -----------------------------
# Ejecución del Programa...
# b = u2_moduloFuncionesValorDefecto.valor()
# print(f"\nRetorno de la función: {b} \n")
print(f"Retorno de la función: {u2_moduloFuncionesValorDefecto.valor()} \n")

print(f"Retorno de la función: {u2_moduloFuncionesValorDefecto.valor(10)} \n")

# a = input("\nDigiten un valor: ")
a = "Hola Tripulantes"
b = u2_moduloFuncionesValorDefecto.valor(a)
print(f"Retorno de la función: {b} \n")