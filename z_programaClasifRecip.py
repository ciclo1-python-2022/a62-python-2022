
# Una empresa que fabrica recipientes cilíndricos, desea automatizar el proceso 
# de clasificación teniendo en cuenta el volumen del recipiente 
# (utilizando lista de datos). 

# La empresa clasifica los productos respecto a la Tabla 1 adjunta. 
# Adicionalmente, empaca  las “Copas” en cajas de 10 unidades, 
# los “Vasos” en cajas de 6 unidades, los “Frascos” en cajas de 
# 40 unidades, las “Botellas peq” en cajas de 30 unidades, 
# las “Botellas med” en cajas de 20 unidades, las “Botellas gran” 
# en cajas de 10 unidades y lo demás “Descartado” en cajas de 100 unidades.

# Programar un sistema que mediante una lista de datos con el volumen 
# de los recipientes, informe su respectiva clasificación, 
# cantidad y número de cajas disponibles.


# ---------------------------------------------------
# Programa a Ejecutar

# importar módulos
# import random
from random import randint, uniform
from z_moduloClasifRecip import volRecipCilindrico, clasificacionRecipientes

# Entradas => Diámetro y Altura del cilindro para calcular el volumen
# estimar una cantidad de recipientes a producir.
# cantRecip = 100
cantRecip = randint(500, 10000)
print(f"\nLista con el volument de los {cantRecip} Recipientes Cilíndricos:")
# listaVolRecip = []
listaVolRecip = list()

for i in range(cantRecip):
    alto = uniform(3, 30) # genera valores aleatorios decimales entre 3 y 30
    diametro = uniform(3, 10)  # genera valores aleatorios decimales entre 3 y 10
    volumen = volRecipCilindrico(diametro, alto)
    listaVolRecip.append(volumen)
    # listaVolRecip.append(volRecipCilindrico(diametro, alto))

print(f"\nLista con el volument de los {cantRecip} Recipientes Cilíndricos:")
print(listaVolRecip, "\n")

# Transformación
# Clasificación de la lista de recipientes...
clasifRecip = clasificacionRecipientes(listaVolRecip)

# Salida
print("\nEl Resultado de la Clasificación de Recipientes es: ")
print(clasifRecip, "\n")