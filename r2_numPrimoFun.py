
# Programa para identificar si un número es primo
# Divisible por 1 y por sí mismo.

# ------------------------------------
# Definir o Declarar la Función
def esNumPrimo(num):

    cont = 0
    if num < 0:
        # num = num * (-1)
        num *= -1
    if num % 2 == 0 and num != 2 or num == 1:
        return False   # Significa que el número no es primo.
    else:
        for i in range(3, (num//2)+1, 2):    ### range(1, (num//2)+1):         ### range(1, num+1):
            print(num, "/", i, "=", num / i)
            # print()
            if num % i == 0:
                cont += 1
                print("cont =", cont)
                return False   # Significa que el número no es primo.
        return True   # Significa que el número es primo.

    # return True o False 


# ------------------------------------
# Ejecución del Programa...
numero = -2

if esNumPrimo(numero):
    print("\nEl número", numero, "es Primo.\n")
else:
    print("\nEl número", numero, "NO es Primo.\n")

# numero = -2
# esPrimo = esNumPrimo(numero)

# if esPrimo:
#     print("\nEl número", numero, "es Primo.\n")
# else:
#     print("\nEl número", numero, "NO es Primo.\n")