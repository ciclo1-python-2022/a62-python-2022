
# Identificar Palíndromos
# Frases o Palabras que se leen igual de izquierda a derecho que de derecha a izquierda.
# oso  -  ana - ojo - ala - anilina -  luz azul - radar - yo hago yoga hoy - anita lava la tina 
# Dábale arroz a la zorra el abad

# -----------------------------------------------
# Declarar o definir funcione(s)
def palindromo(palabraFrase = "Ojo"):
    # palabraFrase = ""
    palabraFrase = palabraFrase.lower()
    palabraFrase = palabraFrase.replace(" ", "")
    palabraFrase = palabraFrase.replace("á", "a")
    palabraFrase = palabraFrase.replace("é", "e")
    palabraFrase = palabraFrase.replace("í", "i")
    palabraFrase = palabraFrase.replace("ó", "o")
    palabraFrase = palabraFrase.replace("ú", "u")
    palabraFrase = palabraFrase.replace("\t", "")
    palabraFrase = palabraFrase.replace(".", "")
    palabraFrase = palabraFrase.replace(",", "")
    palabraFrase = palabraFrase.replace(":", "")
    palabraFrase = palabraFrase.replace(";", "")
    palabraFrase = palabraFrase.replace("-", "")
    palabraFrase = palabraFrase.replace("_", "")
    palabraFraseInvertida = palabraFrase[::-1]

    if palabraFrase == palabraFraseInvertida:
        return True  # La palabra / frase es un palíndromo.
    else:
        return False # La palabra / frase NO es un palíndromo.
    



