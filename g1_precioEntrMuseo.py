
# Programa para identificar el pago de entrada al Museo del Oro según rango de edad
# Si es menor de 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000   => a partir de 60 años.
# Los demás pagan $60.000   => mayor de edad (18) y menor de 60 años.

# Entrada(s)
# edad = 6
edad = int(input("Digite la edad del visitante al Museo: "))

# Transformación
if edad < 12:
    precEntrMuseo = 10000

# elif edad >= 12 and edad < 18:
elif edad < 18:
    precEntrMuseo = 20000

# elif edad >= 18 and edad < 60:
elif edad < 60:
    precEntrMuseo = 60000

else:
    precEntrMuseo = 30000


# Salida(s)
# precEntrMuseo
print("Precio entrada al Museo de Oro es: ", precEntrMuseo)
