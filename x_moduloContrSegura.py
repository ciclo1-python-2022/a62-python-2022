
# Módulo del Programa para Generar Contraseñas Seguras
# Incluir mayúsculas, minúsculas, números, símboles
# Longitud mínima 8 caracteres.

# Importar el módulo de valores aleatorios
# import random
from random import randint, choice

# Declarar o Definir la Función
def generarContrasegnaSeguras():
    # Entradas
    # mayusculas = ["A", "B", "C", "D"...]
    mayusculas = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    # minusculas = mayusculas.lower()  # convierto la cadena de caracteres a minúsculas
    # minusculas = minusculas.split()  # convierto las minúsculas a una lista.
    minusculas = list(mayusculas.lower()) # convierto la cadena de caracteres a minúsculas en una lista.
    # print(f"\nminusculas: {minusculas}")
    mayusculas = list(mayusculas)  # convierte las mayúsculas a una lista.
    # print(f"mayusculas: {mayusculas}")

    numeros = list("1234567890")
    # print(f"numeros: {numeros}")
    simbolos = list("@#$%&/.,+-_*")
    # print(f"simbolos: {simbolos}")

    caracteresUtilizar = numeros + mayusculas + numeros + minusculas + numeros + simbolos
    # print(f"caracteresUtilizar: {caracteresUtilizar}")

    # contrasegna = []
    contrasegna = list()

    # longContr = random.randint(10, 20) # generar un valor aleatorio entre 10 y 20
    longContr = randint(10, 20) # generar un valor aleatorio entre 10 y 20
    # print(f"longContr: {longContr}")

    # Transformación
    for i in range(longContr):
        # caractAleatorio = random.choice(caracteresUtilizar) # selecciono un caracter aleatorio de la lista caractereUtilizar
        caractAleatorio = choice(caracteresUtilizar) # selecciono un caracter aleatorio de la lista caractereUtilizar
        # print(f"caractAleatorio: {caractAleatorio}")
        contrasegna.append(caractAleatorio) # agregar el caracter aleatorio a la lista contrasegna
        # print(f"contrasegna: {contrasegna}")
        # contrasegna.append(random.choice(caracteresUtilizar))
    
    # contrasegna = "".join(contrasegna)
    contrasegna = str().join(contrasegna)
    # print(f"contrasegna: {contrasegna}")

    # Salida
    return contrasegna