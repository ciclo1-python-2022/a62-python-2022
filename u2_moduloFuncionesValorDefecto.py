

# Ejemplo Establecer Volores por defecto en una función

# -----------------------------
# Definir o Declarar la Función

# def valor(parametro):
def valor(parametro = 6):
    print(f"\nEl valor del parámetro = {parametro}")
    c = parametro * 3
    print(f"\nEl valor del parámetro x 3 = {c}")
    # return True
    return c
