print("\nListas:\n")
# --------------------------------------------------
# Listas
# Se declaran con corchetes []
# Son modificables - mutables

l = []  # lista vacía
l = list()  # lista vacía

l = [1, 2, 3, 4, 5, 6]  # lista homogenea de enteros
l = ["a", "2", "b", "4", "z", "6"]  # lista homogénea de string o cadena de caracteres
l = ["a", 2, True, "4", "z", 6.6]  # lista heterogénea
l = ["a", 2, True, "4", "z", 6.6, [3, 2, 1]]  # lista heterogénea incluyento anidación de lista

listNombre = ["Hugo", "Paco", "Luis"]
listTelef = [2222, 3333, 4444]
print(f"listTelef: {listTelef}")

print(f"el Teléfono de {listNombre[0]}, es: {listTelef[0]}")
print(f"el Teléfono de {listNombre[1]}, es: {listTelef[1]}")
print(f"el Teléfono de {listNombre[2]}, es: {listTelef[2]}")

# For recorre la lista, teniendo en cuenta cada uno de los elementos
for varIterable1 in listNombre:
    print(f"El valor de varIterable1 es: {varIterable1}")

# For recorre la lista teniendo en cuenta los subíndices
for varIterable2 in range(len(listNombre)):
    # print(f"El valor de varIterable2 es: {listNombre[varIterable2]}")
    print("El valor de varIterable2 es:", listNombre[varIterable2])

print(f"listTelef: {listTelef}")
listTelef[2] = 6666
listTelef.append(777)
listTelef.append(444)
listTelef.append(888)
print(f"listTelef: {listTelef}")
listTelef.pop(0)
print(f"listTelef: {listTelef}")



print("\nTuplas:\n")
# --------------------------------------------------
# Tuplas
# Se declaran con paréntesis ()
# Son inmutables, es decir, NO se pueden modificar después de ser declaradas o definidas.

tp = ()  # declarar o definir una tupla vacía
tp = tuple()  # declarar o definir una tupla vacía

tp = (1, 2, 3, 4)   # tupla homogénea de valores enteros.
tp = ("a", "2", "b", "4", "z", "6") # tupla homogénea de valores string.
tp = ("a", 2, True, "4", "z", 6.6) # tupla heterogénea
tp = ("a", 2, True, "4", "z", 6.6, [3, 2, 1]) 
ls = [1,tp, 3]

tp1 = ("Hugo", 111)
tp2 = ("Paco", 222)
tp3 = ("Luis", 333)

print(f"tp1: {tp1}")
print(f"tp2: {tp2}")
print(f"tp3: {tp3}")

tp = ("a", 2, True, "4", "z", 6.6, [3, 2, 1]) 
print("\nAntes del For...")
for varIter in range(len(tp)):
    print(f"El valor de varIter = {varIter}, el contenido de la tupla tp[{varIter}] es: {tp[varIter]}")
    print("El valor de varIter = " + str(varIter) + ", el contenido de la tupla tp[" + str(varIter) + "] es: " + str(tp[varIter]))
    print("El valor de varIter =", varIter, ", el contenido de la tupla tp[", varIter, "] es:", tp[varIter])
print("Después del For...\n")


# ---------------------------

# def valor():
#     a = 123
#     b = [6, 1, "a", True, 2.6]
#     return a, b

# x, y = valor()
# print(f"\nx = {x}")
# print(f"y = {y}\n")

# a, b = valor()
# print(f"\na = {a}")
# print(f"b = {b}\n")

# b, a = valor()
# print(f"\na = {a}")
# print(f"b = {b}\n")

# def valor2():
#     a = 123
#     b = [6, 1, "a", True, 2.6]
#     c = d
#     print(f"c = d = {c}")
#     return a, b

# d = "Hola Tripulantes";
# a, b = valor2();print(f"\na = {a}"); print(f"b = {b}\n")




# --------------------------------------------------
# Diccionarios
# Se declaran con llaves {}
# Son modificables - mutables
# Cada elemento dentro del diccionario se identifica con una llave (key) con su respectivo valor (value)

dc = {}   # declarar o definir un diccionario vacío
dc = dict() # declarar o definir un diccionario vacío

# Ejemplo de un diccionario
dcTrip1 = {
    "Nombre": "Hugo",   # primer elemento (item) del diccionario, tiene como llave (key) Nombre y como valor (value) Hugo
    "CC": 123456,   # segundo elemento (item) del diccionario, tiene como llave (key) CC y como valor (value) 123456
    "Tel": 555
}

print(dcTrip1)

dcTrip2 = {"Nombre": "Paco", "CC": 654321, "Tel": 666}
print(dcTrip2)

dcTrip1 = {
    "Nombre": "Luis",
    "CC": 12345600,
    "Tel": 555000,
    321: 123
}
print(dcTrip1)

print("\nAntes del For...")
for varIter in dcTrip1:
    print(f"La clave o llave en varIter es: {varIter}, el valor de dcTrip1[{varIter}] es: {dcTrip1[varIter]}")
print("Después del For...\n")


print("\nAntes del For...")
for varIter in dcTrip1.keys():
    print(f"La clave o llave en varIter es: {varIter}, el valor de dcTrip1[{varIter}] es: {dcTrip1[varIter]}")
print("Después del For...\n")


print("\nAntes del For...")
for varIter in dcTrip1.values():
    print(f"Valor de dcTrip1 = {varIter}")
print("Después del For...\n")


print("\nAntes del For...")
for claveDc, valorDc in dcTrip1.items():
    print(f"La clave es: {claveDc}, y el valor es: {valorDc}")
print("Después del For...\n")


print(f"\nImprime únicamente las claves (keys): {dcTrip2.keys()}")
print(f"Imprime únicamente las valores (values): {dcTrip2.values()}")
print(f"Imprime claves (keys) y valores (values): {dcTrip2.items()}\n")



paisCapital = {
    "Colombia": {"Capital": "Bogotá",
                 "CantHabit": 50366666,
                 "asnm": 2600
                },
    "México": "México D.F.",
    "Argentina": "Buenos Aire"
}


