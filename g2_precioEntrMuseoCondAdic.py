
# Programa para identificar el pago de entrada al Museo del Oro según rango de edad
# Si es menor de 12 años paga $10.000
# Si la edad está entre 12 y 17 años paga $20.000
# Si es adulto mayor, paga $30.000   => a partir de 60 años.
# Los demás pagan $60.000   => mayor de edad (18) y menor de 60 años.
# Si el visitante tiene una estura mayor a metro y medio, incrementa el costo en $3.000
# Si es fin de semana o festivo el costo de la entrada incrementa en 30%

# Entrada(s)
fds = input("\nEs fin de semana o festivo (S/N): ")
if fds == "S" or fds == "s":
    fds = True
else:
    fds = False
# edad = 6
edad = int(input("\nDigite la edad del visitante al Museo: "))
estatura = float(input("Digite la estura del visitante (m): "))

# Transformación
if edad < 12:
    precEntrMuseo = 10000

# elif edad >= 12 and edad < 18:
elif edad < 18:
    precEntrMuseo = 20000

# elif edad >= 18 and edad < 60:
elif edad < 60:
    precEntrMuseo = 60000

else:
    precEntrMuseo = 30000

if estatura > 1.5:
    # precEntrMuseo = precEntrMuseo + 3000
    precEntrMuseo += 3000

# if fds == True:
if fds:
    # precEntrMuseo = precEntrMuseo + (precEntrMuseo * 30 / 100)
    # precEntrMuseo = precEntrMuseo * (1 + 0.3)
    # precEntrMuseo = precEntrMuseo * (1.3)
    precEntrMuseo *= (1.3)

# Salida(s)
# precEntrMuseo
print("\nPrecio entrada al Museo de Oro es: ", precEntrMuseo, "\n")
