
# Programa para identificar si una persona es mayor de edad

# Entrada(s)
rta = 0
# edad = input("Digite su edad: ")
# edad = int(edad)
edad = int(input("Digite su edad: "))

# Transformación
if edad > 17:
    print("Pasó por el primer if edad > 17")
if edad >= 18:
    rta = "El usuario es mayor de edad."
    print("Pasó por la respuesta verdadera.")
else:
    rta = "El usuario es menor de edad."
    print("Pasó por la respuesta falsa.")


# Salida(s)
print(rta)