
# Programa para generar una tabla de multiplicación

tablaMultip = 1
menu = """
######################################################################

Para finalizar el programa digite un valor negativo o cero(0)

Digite el número hasta el cual desea generar las tablas de multiplicar: """

while tablaMultip > 0:

    tablaMultip = int(input(menu))
    # print("###### Tabla de Multiplicar del", tablaMultip, "######")
    for multiplicando in range(1, tablaMultip + 1):
        print("\n\n###### Tabla de Multiplicar del", multiplicando, "######\n")
        for multiplicador in range(1, 11):
            print(multiplicando, "x", multiplicador, "=", multiplicando * multiplicador)
    
    
print("\n######################################################################")
print("\nFin del Programa...\n")



# Analizar => En qué valor finaliza el while y el for en Python