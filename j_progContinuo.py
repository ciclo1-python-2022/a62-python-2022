
# Programa para ejecución continua hasta que el usuario desee terminar


menu = """
####### Menú de Opciones ######

"Seleccione un valor numérico de las siguientes opciones:"

1. Primer caso.
2. Segundo caso.
3. Tercer caso.
4. Cuarto caso.

0. Terminar o Salir: """

# menuOpcion = ""
menuOpcion = str()

while menuOpcion != "0":
    # pass
    # menuOpcion = input("\nDigite un valor: ")
    menuOpcion = input(menu)
    
    print("\n###########################################################")

    if menuOpcion == "1":
        print("\nSeleccionó la opción 1...\n")
    elif menuOpcion == "2":
        print("\nSeleccionó la opción 2...\n")
    elif menuOpcion == "3":
        print("\nSeleccionó la opción 3...\n")
    elif menuOpcion == "4":
        print("\nSeleccionó la opción 4...\n")
    elif menuOpcion == "0":
        print("\nSeleccionó la opción de Terminar o Salir")
        menuOpcion = input("\nPor favor confirme (S/N): ")
        if menuOpcion == "S" or menuOpcion == "s" or menuOpcion == "":
            menuOpcion = "0"
    else:
        print("\nPor favor, seleccione una opción válida...\n")


print("\nFin del programa...\n")









