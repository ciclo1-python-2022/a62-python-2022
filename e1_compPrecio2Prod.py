
# Programa para comparar el precio de 2 productos

# Entrada(s)
precioProd1 = 5
precioProd2 = 6


# Tranformación - Salida(s)

print("------")
print("Inicia if precioProd1 < precioProd2")
if precioProd1 < precioProd2:
    print("La respuesta es True")
    print("El precio del producto 1 es menor que el precio del producto 2")
else:
    print("La respuesta es False")
    print("El precio del producto 1 NO es menor que el precio del producto 2")
print("fin if precioProd1 < precioProd2")

print("------")

print("Inicia if precioProd1 > precioProd2")
if precioProd1 > precioProd2:
    # pass
    print("La respuesta es True")
    print("El precio del producto 1 es mayor que el precio del producto 2")
else:
    # pass
    print("La respuesta es False")
    print("El precio del producto 1 NO es mayor que el precio del producto 2")
print("fin if precioProd1 > precioProd2")

print("------")

print("Inicia if precioProd1 <= precioProd2")
if precioProd1 <= precioProd2:
    # pass
    print("La respuesta es True")
    print("El precio del producto 1 es menor o igual que el precio del producto 2")
else:
    # pass
    print("La respuesta es False")
    print("El precio del producto 1 NO es menor o igual que el precio del producto 2")
print("fin if precioProd1 <= precioProd2")

print("------")

print("Inicia if precioProd1 >= precioProd2")
if precioProd1 >= precioProd2:
    # pass
    print("La respuesta es True")
    print("El precio del producto 1 es mayor o igual que el precio del producto 2")
else:
    # pass
    print("La respuesta es False")
    print("El precio del producto 1 NO es mayor o igual que el precio del producto 2")
print("fin if precioProd1 >= precioProd2")

print("------")

print("Inicia if precioProd1 == precioProd2")
if precioProd1 == precioProd2:
    # pass
    print("La respuesta es True")
    print("El precio del producto 1 es igual al precio del producto 2")
else:
    # pass
    print("La respuesta es False")
    print("El precio del producto 1 NO es igual al precio del producto 2")
print("fin if precioProd1 == precioProd2")

print("------")

print("Inicia if precioProd1 != precioProd2")
if precioProd1 != precioProd2:
    # pass
    print("La respuesta es True")
    print("El precio del producto 1 es diferente al precio del producto 2")
else:
    # pass
    print("La respuesta es False")
    print("El precio del producto 1 NO es diferente al precio del producto 2")
print("fin if precioProd1 != precioProd2")

print("------")

a = False
if not a:
    # pass
    print("Contenido a la respuesta falsa")
# else:
#     print("Contenido a la respuesta falsa")