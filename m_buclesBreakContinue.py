
# Explicación Break y Continue en Bucles


# --------------------- Break ----------------------

print("\nAntes de iniciar el for...\n")
for i in range(10):
    print("i =", i)
    # break
    if i == 3:
        print("Antes  del break...")
        break
        print("Después del break...")
    print("Después del condicional...")

print("\nSalió del for...\n")



print("\nAntes de iniciar el while...\n")
cont = 0
while cont < 10:
    print("cont =", cont)
    # break
    if cont == 3:
        print("Antes  del break...")
        break
        print("Después del break...")
    print("Después del condicional...")
    cont += 1
print("\nSalió del while...\n")



# --------------------- Continue ----------------------

print("\nAntes de iniciar el for...\n")
for i in range(10):
    print("i =", i)
    # break
    if i == 3:
        print("Antes  del continue...")
        continue
        print("Después del continue...")
    print("Después del condicional...")

print("\nSalió del for...\n")



print("\nAntes de iniciar el while...\n")
cont = 0
while cont < 10:
    print("cont =", cont)
    # break
    cont += 1
    if cont == 3:
        print("Antes  del continue...")
        continue
        print("Después del continue...")
    print("Después del condicional...")
    # cont += 1
print("\nSalió del while...\n")


# ---------------------------------------------
print("\nMúltiplos de 4 hasta el 20\n")
sumatoria = 0
for i in range (21):
    if i % 4 == 0:
        print("Valor i =", i, "es múltiplo de 4...")
        continue
    print("i = ", i)
    sumatoria += i
    print("Sumatoria =", sumatoria)
    


print("\nTerminó for...\n")