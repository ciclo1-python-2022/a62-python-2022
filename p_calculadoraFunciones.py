
# Calculadora Utilizando Funciones

# ------- Definir o Declarar Funciones --------

def suma2Num(parametro1, parametro2):
    resultado = parametro1 + parametro2
    return resultado

def resta2Num(parametro1, parametro2):
    resultado = parametro1 - parametro2
    # if parametro1 > parametro2:
    #     resultado = parametro1 - parametro2
    # else:
    #     resultado = parametro2 - parametro1
    return resultado

def multip2Num(parametro1, parametro2):
    resultado = parametro1 * parametro2
    return resultado

def div2Num(parametro1, parametro2):
    resultado = parametro1 / parametro2
    return resultado


# ------- Inicio del Programa --------
# Entrada
# a = 5
a = float(input("\nDigite el valor de a: "))
# b = 6
b = float(input("\nDigite el valor de b: "))

# Transformación
# c = a + b
c = suma2Num(a, b)
d = resta2Num(a, b)
e = multip2Num(c, d)
f = div2Num(c, d)


f = round(div2Num(c, d), 2)


# Salida
print("\nc = ", a, "+", b, "=", c, "\n")
print("\nd = ", a, "-", b, "=", d, "\n")
print("\ne = ", c, "x", d, "=", e, "\n")
print("\nf = ", c, "/", d, "=", round(f, 2), " Redondeo directamente en la impresión...\n")
f = round(f, 2)
print("\nf = ", c, "/", d, "=", f, " redendeo previamente actualizando la variable\n")

print("\nLlamado a la función directa 100 + 60 = ", suma2Num(100, 60))

# print(4+3, 2)