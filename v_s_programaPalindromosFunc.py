
# Identificar Palíndromos
# Frases o Palabras que se leen igual de izquierda a derecho que de derecha a izquierda.
# oso  -  ana - ojo - ala - anilina -  luz azul - radar - yo hago yoga hoy - anita lava la tina 
# Dábale arroz a la zorra el abad

# -----------------------------------------------
# Importar el módulo de palindromo
# import v_s_moduloPalindromosFunc
from v_s_moduloPalindromosFunc import palindromo
from math import pi, sqrt, pow
# import math

# -----------------------------------------------
# Programa a ejecutar...

frase = ""

while frase.lower().strip() != "salir":
    # frase = "Tripulantes"
    frase = input("\nDigite una palabra o frase palíndromo (\"salir\" para terminar): \n")
    # if v_s_moduloPalindromosFunc.palindromo(frase):
    if palindromo(frase):
        print("\nLa palabra o frase \"" + frase + "\" es un Palíndromo.\n")
    else:
        print("\nLa palabra o frase \"" + frase + "\" NO es un Palíndromo.\n")



print(f"\n5 elevado al cuadrado es igual a {pow(5, 2)}")
print(f"\nEl valor de PI es: {pi}")
print(f"\nLa raiz cuadrada de 100 es: {sqrt(100)} \n")

# a = "Tripulantes"
# a.strip()
# a.replace()

