
# Programa para identificar si un número es primo
# Divisible por 1 y por sí mismo.

# ------------------------------------
# Definir o Declarar la Función
def esNumPrimo(num):

    cont = 0
    if num < 0:
        # num = num * (-1)
        num *= -1
    if num % 2 == 0 and num != 2 or num == 1:
        # return False   # Significa que el número no es primo.
        primo = False
    else:
        primo = True
        for i in range(3, (num//2)+1, 2):    ### range(1, (num//2)+1):         ### range(1, num+1):
            print(num, "/", i, "=", num / i)
            # print()
            if num % i == 0:
                cont += 1
                print("cont =", cont)
                # return False   # Significa que el número no es primo.
                primo = False
                break
        # return True   # Significa que el número es primo.
    return primo


# ------------------------------------
# # Ejecución del Programa...
# numero = -2
numeroRango = int(input("\nDigite un número entero: "))

numPrimo = ""

for numero in range(numeroRango + 1):
    if esNumPrimo(numero):
        print("El número", numero, "es Primo.")
        numPrimo += str(numero) + ", "
    else:
        print("El número", numero, "NO es Primo.")

print("\nNúmeros Primos: ")
print(numPrimo)

print("\nFin del Programa.\n")

