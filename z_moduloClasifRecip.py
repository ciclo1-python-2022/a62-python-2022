
# --------------------------------------------------------------
# Función para Calcular el Volumen de un Recipiente Cilíndrico
def volRecipCilindrico(diametro = 0, altura = 0):
    from math import pi
    volumen = pi * (diametro / 2) ** 2 * altura
    volumen = round(volumen, 2)
    # volumen = round(pi * (diametro / 2) ** 2 * altura, 2)
    return volumen



# --------------------------------------------------------------
# Función para Clasificar los Recipientes
# llega una lista con el volumen de los recipientes = [60, 40, 80, 100, 200, 600, ...]
def clasificacionRecipientes(listaVolRecip):

    tipoRecipCopa = {
        "TipoRecipiente": "Copa",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipVaso = {
        "TipoRecipiente": "Vaso",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipFrasco = {
        "TipoRecipiente": "Frasco",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipBotPeq = {
        "TipoRecipiente": "BotPeq",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipBotMed = {
        "TipoRecipiente": "BotMed",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipBotGran = {
        "TipoRecipiente": "BotGran",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    tipoRecipDescartado = {
        "TipoRecipiente": "Descartado",
        "CantRecip": 0,
        "CantCajas": 0,
        "CantPendXCaja": 0
    }

    for volRecip in listaVolRecip:

        if volRecip >= 40 and volRecip <= 60:
            # tipoRecipCopa["CantRecip"] = tipoRecipCopa["CantRecip"] + 1
            tipoRecipCopa["CantRecip"] += 1
            if tipoRecipCopa["CantRecip"] % 10 == 0:
                tipoRecipCopa["CantCajas"] += 1
        elif volRecip >= 80 and volRecip <= 120:
            tipoRecipVaso["CantRecip"] += 1
            if tipoRecipVaso["CantRecip"] % 6 == 0:
                tipoRecipVaso["CantCajas"] += 1
        elif volRecip >= 140 and volRecip <= 200:
            tipoRecipFrasco["CantRecip"] += 1
            if tipoRecipFrasco["CantRecip"] % 40 == 0:
                tipoRecipFrasco["CantCajas"] += 1
        elif volRecip >= 230 and volRecip <= 280:   # volRecip == 250:
            tipoRecipBotPeq["CantRecip"] += 1
            if tipoRecipBotPeq["CantRecip"] % 30 == 0:
                tipoRecipBotPeq["CantCajas"] += 1
        elif volRecip >= 300 and volRecip <= 500:
            tipoRecipBotMed["CantRecip"] += 1
            if tipoRecipBotMed["CantRecip"] % 20 == 0:
                tipoRecipBotMed["CantCajas"] += 1
        elif volRecip >= 600 and volRecip <= 1000:
            tipoRecipBotGran["CantRecip"] += 1
            if tipoRecipBotGran["CantRecip"] % 10 == 0:
                tipoRecipBotGran["CantCajas"] += 1
        else:
            tipoRecipDescartado["CantRecip"] += 1
            if tipoRecipDescartado["CantRecip"] % 100 == 0:
                tipoRecipDescartado["CantCajas"] += 1

    # tipoRecipCopa["CantPendXCaja"] = tipoRecipCopa["CantRecip"] - tipoRecipCopa["CantCajas"] * 10
    tipoRecipCopa["CantPendXCaja"] = tipoRecipCopa["CantRecip"] % 10
    tipoRecipVaso["CantPendXCaja"] = tipoRecipVaso["CantRecip"] % 6
    tipoRecipFrasco["CantPendXCaja"] = tipoRecipFrasco["CantRecip"] % 40
    tipoRecipBotPeq["CantPendXCaja"] = tipoRecipBotPeq["CantRecip"] % 30
    tipoRecipBotMed["CantPendXCaja"] = tipoRecipBotMed["CantRecip"] % 20
    tipoRecipBotGran["CantPendXCaja"] = tipoRecipBotGran["CantRecip"] % 10
    tipoRecipDescartado["CantPendXCaja"] = tipoRecipDescartado["CantRecip"] % 100

    clasifRecip = [tipoRecipCopa, tipoRecipVaso, tipoRecipFrasco, tipoRecipBotPeq, tipoRecipBotMed, tipoRecipBotGran, tipoRecipDescartado]
    return clasifRecip


